# Experimental evaluation of boomerang distinguishers on Twine

This repository provides a tool to experimentally check the probability of a boomerang distinguisher on Twine for both 80 and 128 bit versions.

**NOTE:** To simplify the [DBLP:journals/tosc/LallemandMR22](https://tosc.iacr.org/index.php/ToSC/article/view/9716) model, the authors have added a permutation at the end of the encryption. 
Depending on how the boomerang to be tested is constructed, this extra permutation may have to be removed.

## Usage

This code is written in [Rust](https://www.rust-lang.org/learn/get-started) and can be compiled with `cargo`.

```sh
cargo run --release -- -d [DISTINGUISHER PATH]
```

For example:
```sh
cargo run --release -- -d distinguishers/3-round-80bits.json
```

The distinguisher files are in `.json` format:
```json
{
  "nr": 3,
  "key_len": 80,
  "input_difference": [1, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  "output_difference": [null, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1],
  "log2_nb_keys": 4,
  "log2_nb_tries_by_key": 4
}
```

- `nr` is the number of round of the distinguisher,
- `key_len` is the number of bits of the key and **MUST** be either 80 or 128,
- `input_difference` is the input difference vector,
- `output_difference` is the output difference vector,
- `log2_nb_keys` is the $`\log_2`$ number of keys to try (the number of tried keys will be $`2^{log2\_nb\_keys}`$)
- `log2_nb_tries_by_key` is the $`\log_2`$ number of pairs of plaintext tried for each key.

If a nibble can take any non-zero value uniformly, the user can set a `null` instead of the nibble value. The value will be set by the random generator. 

## Tests

This code has been tested on the test vectors given in [DBLP:conf/sacrypt/SuzakiMMK12](https://link.springer.com/chapter/10.1007/978-3-642-35999-6_22). The tests can be executed with:

```sh
cargo test
```

@article{DBLP:journals/tosc/LallemandMR22,
author    = {Virginie Lallemand and
Marine Minier and
Lo{\"{\i}}c Rouquette},
title     = {Automatic Search of Rectangle Attacks on Feistel Ciphers: Application
to {WARP}},
journal   = {{IACR} Trans. Symmetric Cryptol.},
volume    = {2022},
number    = {2},
pages     = {113--140},
year      = {2022}
}

@inproceedings{DBLP:conf/sacrypt/SuzakiMMK12,
author    = {Tomoyasu Suzaki and
Kazuhiko Minematsu and
Sumio Morioka and
Eita Kobayashi},
title     = {{\textdollar}{\textbackslash}textnormal\{{\textbackslash}textsc\{TWINE\}\}{\textdollar}
: {A} Lightweight Block Cipher for Multiple Platforms},
booktitle = {Selected Areas in Cryptography},
series    = {Lecture Notes in Computer Science},
volume    = {7707},
pages     = {339--354},
publisher = {Springer},
year      = {2012}
}